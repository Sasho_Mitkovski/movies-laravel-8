@extends('layouts.app')
@section('content')
<div class="container">

    <div class="card w-25">
        <div class="card-body">
          <h5 class="card-title fw-bold">{{ $movie->title }}</h5>
          <p class="card-text">{{ $movie->date_released }} </p>
          <p class="fw-bold">{{ $movie->duration }} min </p>
          <p class="fw-bold">{{ $movie->genre_id }} </p>
       <p> <a href="{{ route('movies.edit', $movie->id) }}" class="btn btn-success">Edit?</a></p>
          {{-- <a href="/movies" class="btn btn-secondary">Nazad </a> --}}
        </div>
      </div>
    {{-- <td>{{ $movie->id }}</td><br> --}}

</div>
@endsection
