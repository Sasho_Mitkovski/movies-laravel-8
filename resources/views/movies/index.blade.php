@extends('layouts.app')
@section('title', 'movies')
@section('content')
<div class="container">

<table class="table">
<thead>
    <th>Id</th>
    <th>Title</th>
    <th>Description</th>
    <th>Date released</th>
    <th>Duration</th>
    <th>Genre</th>
    <th>Details</th>
    <th>Delete movie</th>
</thead>
<tbody>
@foreach($movies as  $movie)
<tr>
<td>{{ $movie->id }}</td>
<td class="fw-bold">{{ $movie->title }}</td>
    <td>{{ $movie->description }} </td>
<td>{{ $movie->date_released }} </td>
<td class="fw-bold">{{ $movie->duration }} min</td>
<td>{{ $movie->genre_id }}</td>
<td class="fw-bold"><a class="btn btn-success" href="movies/show/{{ $movie->id }}">details</td>
<td>
    <form action="{{ route('movies.delete',$movie->id) }}" method="POST">
        @csrf
<button type="submit" class="btn btn-danger">Delete</button>
    </form>
</td>
</tr>
@endforeach


</tbody>
</table>

</div>

@endsection
