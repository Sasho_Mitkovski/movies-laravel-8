@extends('layouts.app')
@section('title', 'create')
@section('content')
    <div class="container">
        <div class="col-md-4">
            <form action="{{ route('movies.store') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="title" class="form-label">Title</label>
                    <input type="text" class="form-control" id="title" name="title" aria-describedby="title">
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea type="text" class="form-control" id="description" name="description"
                        aria-describedby="description"></textarea>
                </div>
                <div class="mb-3">
                    <label for="date_released" class="form-label">Date released</label>
                    <input type="date" class="form-control" id="date_released" name="date_released"
                        aria-describedby="date_released">
                </div>
                <div class="mb-3">
                    <label for="duration" class="form-label">Movie duration ( in minutes )</label>
                    <input type="text" class="form-control" id="duration" name="duration" aria-describedby="duration">
                </div>
                <div class="mb-3">
                    <label for="genre_id" class="form-label">Genre</label>
                    <select class="form-control" id="genre_id" name="genre_id">
                        <option selected disabled>Select genre</option>
                        @foreach ($genres as $genre)
                            <option value="{{ $genre->id }}">{{ ucfirst($genre->name) }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
