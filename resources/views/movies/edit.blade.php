
    @extends('layouts.app')
    @section('content')
    <div class="container">
        <div class="col-md-4">
        <form action="{{ route('movies.update', $movie->id) }}" method="POST">
            @csrf
            <div class="mb-3">
              <label for="title" class="form-label">Title</label>
              <input type="text" class="form-control" id="title" name="title" aria-describedby="title" value="{{$movie->title }}">
            </div>
            <div class="mb-3">
              <label for="description" class="form-label">Description</label>
              <textarea type="text" class="form-control" id="description"  name="description" aria-describedby="description" value="">{{$movie->description  }}</textarea>
            </div>
            <div class="mb-3">
              <label for="date_released" class="form-label">Date released</label>
              <input type="date" class="form-control" id="date_released"  name="date_released" aria-describedby="date_released" value="{{$movie->date_released }}">
            </div>
            <div class="mb-3">
              <label for="duration" class="form-label">Movie duration ( in minutes )</label>
              <input type="text" class="form-control" id="duration"  name="duration" aria-describedby="duration" value="{{$movie->duration }}">
            </div>
            <div class="mb-3">
                <label for="genre_id" class="form-label">Genre</label>
              <select class="form-control" id="genre_id"  name="genre_id">
                <option selected disabled>Select genre</option>
           @foreach($genres as $genre)
                 <option value="{{ $genre->id }}" {{ $movie->genre_id == $genre->id ? 'selected' : ' ' }} >{{ucfirst($genre->name)  }}</option>
           @endforeach
            </select>
            </div>

            <button type="submit" class="btn btn-primary">Updated</button>
          </form>
        </div>
    </div>
    @endsection



