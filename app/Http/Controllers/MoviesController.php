<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MoviesController extends Controller
{
    public function index()
    {

        $movies = DB::table('movies')->orderByDesc('id')->get();

        return view('/movies/index', [
            'movies' => $movies,
        ]);
    }

    public function show($id)
    {

        $movie = DB::table('movies')->where('id', $id)->first();

        return view('movies.show', [
            'movie' => $movie,
        ]);

    }

    public function create()
    {
        $genres = DB::table('genres')->get();
        return view('movies.create', [
            'genres' => $genres,
        ]);
    }

    public function store(Request $request)
    {

        DB::table('movies')->insert([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'date_released' => $request->input('date_released'),
            'duration' => $request->input('duration'),
            'genre_id' => $request->input('genre_id'),
        ]);

        return redirect()->route('movies.index');

    }

    public function edit($id)
    {
        $genres = DB::table('genres')->get();
        $movie = DB::table('movies')->where('id', $id)->first();

        return view('movies.edit', [
            'movie' => $movie,
            'genres' => $genres,
        ]);
    }

    public function update(Request $request, $id)
    {
DB::table('movies')->where('id', $id)->update([
    'title' => $request->input('title'),
    'description' => $request->input('description'),
    'date_released' => $request->input('date_released'),
    'duration' => $request->input('duration'),
    'genre_id' => $request->input('genre_id'),
]);
return redirect()->route('movies.show', $id);
    }

    public function delete($id) {
        DB::table('movies')->where('id', $id)->delete();
        return redirect()->route('movies.index');
    }
}
